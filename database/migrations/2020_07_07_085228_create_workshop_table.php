<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateWorkshopTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('Workshop', function (Blueprint $table) {
            $table->increments('workshop_id');
            $table->string('name')->unique();
            $table->longText('description');
            $table->integer('moderator_id')->unsigned();
            $table->foreign('moderator_id')->references('user_id')->on('User')->onDelete('cascade');
            $table->string('class_room');
            $table->unsignedInteger('duration')->nullable()->default(null);
            $table->unsignedInteger('max_places');
            $table->unsignedInteger('available_places');
            $table->timestamp('start_date');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('Workshop');
    }
}
