<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateWorkshopUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('Workshop_User', function (Blueprint $table) {
            $table->increments('workshop_user_id');
            $table->integer('user_id')->unsigned();
            $table->foreign('user_id')->references('user_id')->on('User')->onDelete('cascade');
            $table->integer('workshop_id')->unsigned();
            $table->foreign('workshop_id')->references('workshop_id')->on('Workshop')->onDelete('cascade');
            $table->unsignedTinyInteger('is_present')->default(0);
            $table->timestamps();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('Workshop_User');
    }
}
