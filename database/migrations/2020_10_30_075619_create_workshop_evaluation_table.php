<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateWorkshopEvaluationTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('Workshop_Evaluation', function (Blueprint $table) {
            $table->increments('workshop_evaluation_id');
            $table->integer('item_evaluation_id')->unsigned();
            $table->foreign('item_evaluation_id')->references('item_evaluation_id')->on('Item_Evaluation')->onDelete('cascade');
            $table->integer('workshop_id')->unsigned();
            $table->foreign('workshop_id')->references('workshop_id')->on('Workshop')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('Workshop_Evaluation');
    }
}
