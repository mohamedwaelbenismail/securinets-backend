<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::middleware('auth:api')->get('/user', function (Request $request) {
//     return $request->user();
// });



Route::group([

    'prefix' => 'auth',

], function () {

    Route::post('login', 'AuthController@login');
    Route::get('logout', 'AuthController@logout');
    Route::post('refresh', 'AuthController@refresh');
    Route::get('me', 'AuthController@me');
 
});
Route::group([
    'prefix' =>'users'
], function() {
Route::post('password/reset/','UserController@forgetPassword');
Route::post('contact-us','UserController@contact');
Route::post('password/reset/{userId}/{verification_code}','UserController@resetPassword');
Route::post('register','UserController@registerUser');
Route::post('update-info','UserController@updateUser')->middleware('assign.guards.users');
Route::get('photo/{path}','UserController@getUserPhoto');
Route::get('emailConfirmation','UserController@emailConfirmation');
Route::get('byId/{userId}','UserController@getUserById');
Route::get('byId/{userId}/verificationCode/{verification_code}','UserController@getUserByIdAndVerificationCode');
Route::get('isAdmin', 'UserController@isAdmin')->middleware('assign.guards.admins');
Route::get('getUserByToken', 'UserController@getUserByToken')->middleware('assign.guards.users');
Route::get('all/{privilege_id}', 'UserController@getAllUserByPrivilege')->middleware('assign.guards.users');

});
///////////////////////////WORKSHOP/////////////////////////////
Route::group(['prefix' => 'workshop'], function () {
    Route::get('photo/{path}','WorkshopController@getWorkshopPhoto');
    Route::get('all', 'WorkshopController@getAllWorkshops');
    Route::get('detail/{workshop_id}', 'WorkshopController@getWorkshopById');
    Route::get('room/{workshop_id}','WorkShopController@createRoomAuth');
    Route::group(['middleware' => 'assign.guards.admins'], function() {
        Route::delete('delete/{workshop_id}', 'WorkshopController@deleteWorkShop');
        Route::post('add', 'WorkshopController@createWorkShop');
        Route::post('edit/{workshop_id}', 'WorkshopController@editWorkShop');
        Route::get('me', 'WorkshopController@getMyPublishedWorkshops');
        Route::get('get-users/{workshopId}', 'WorkshopController@getAllUsersByWorkshopId');
        Route::post('save-presence/{workshop_id}', 'WorkshopController@savePresence');


    });

    Route::group(['middleware' => 'assign.guards.users'], function() {
        Route::post('join', 'WorkshopController@joinWorkshop');
        Route::delete('quit/{workshopId}', 'WorkshopController@quitWorkshop');
    });
});
////////////////////////////////////////////////////////////////////



Route::group(['prefix' => 'user' ,'middleware' => 'assign.guards.users'], function () {
    Route::get('workshops', 'UserController@getUserWorkshops');
});
