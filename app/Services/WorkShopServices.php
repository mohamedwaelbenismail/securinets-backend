<?php

namespace App\Services;

use App\Models\Workshop;
use App\Models\User;
use App\Models\workshop_user_evaluation;
use App\Models\WorkshopUser;
use Illuminate\Http\Request;
use \Firebase\JWT\JWT;
class WorkshopServices
{

    public function addWorkshop($name, $description, $class_room, $start_date, $duration, $path, $max_places, $moderator_id)
    {
        $workShop = new Workshop();
        $workShop->name = $name;
        $workShop->description = $description;
        $workShop->class_room = $class_room;
        $workShop->start_date = $start_date;
        $workShop->duration = $duration;
        $workShop->description = $description;
        $workShop->max_places = $max_places;
        $workShop->url_photo = $path;
        $workShop->available_places = $max_places;
        $workShop->moderator_id = $moderator_id;
        $workShop->save();
        return $workShop;
    }

    public function affectWorkshopToUSer($user_id, $workShop_id, $workshop)
    {
        $workshop->available_places = $workshop->available_places - 1;
        $workShop_user = new WorkshopUser();
        $workShop_user->workshop_id = $workShop_id;
        $workShop_user->user_id = $user_id;
        $workShop_user->save();
        $workshop->update();
        foreach($workshop->item_evaluation as $item_evaluation) {
            $workshop_user_evaluation = new workshop_user_evaluation();
            $workshop_user_evaluation->item_evaluation_id = $item_evaluation->item_evaluation_id;
            $workshop_user_evaluation->workshop_user_id = $workShop_user->workshop_user_id;
            $workshop_user_evaluation->save();
        }
        return $workShop_user;
    }

    public function getWorkShopById($workShop_id)
    {

        return Workshop::with(['moderator:user_id,first_name,last_name',
            'workshop_users' => function ($query) {
                $query->with(['user:user_id,first_name,last_name']);
                $query->with('workshop_user_evaluation');
            },'item_evaluation'
        ])->where('workshop_id', '=', $workShop_id)->first();

    }

    public function editWorkShop($oldWorkShop, $name, $description, $class_room, $start_date, $duration, $path)
    {
        $oldWorkShop->name = $name;
        $oldWorkShop->description = $description;
        $oldWorkShop->class_room = $class_room;
        $oldWorkShop->start_date = $start_date;
        $oldWorkShop->duration = $duration;
        $oldWorkShop->url_photo = $path ? $path : $oldWorkShop->url_photo;
        $oldWorkShop->update();
        return $oldWorkShop;

    }

    public function getWorkShopsByModeratorId($moderator_id)
    {

        return Workshop::where('moderator_id', '=', $moderator_id)->get();
    }

    public function getFirstWorkShopsByModeratorId($moderator_id) {
        return Workshop::where('moderator_id', '=', $moderator_id)
        ->with('workshop_users')
        ->first();
    }

    public function getUserByUserIdByWorkshopId($user_id, $workshop_id)
    {
        return WorkshopUser::where('user_id', '=', $user_id)->where('workshop_id', '=', $workshop_id)->first();
    }

    public function getAllWorkshops($offset, $perPage, $search, $user_id,$moderator_id)
    {
          $workShops =   Workshop::where('name', 'LIKE', '%' . $search . '%')
            ->orderBy('start_date','DESC')
            ->offset($offset)->limit($perPage)
            ->when($moderator_id!=='null',function($query) use ($moderator_id) {
                $query->where('moderator_id','=',$moderator_id);
            } );
            if (!$user_id) {
           
            $workShops =  $workShops->get();      
            foreach($workShops as $workshop) {
                $workshop['workshop_users'] = [];
            }
            return $workShops;
            } else {
                return $workShops->with(['workshop_users'=> function ($query) use ($user_id) {
                    $query->where('user_id','=',$user_id);
                }])->get();
            }
        
    }
    public function createToken($email, $name, $isModerator, $userName)
    {
        $key = env('SECRET_KEY_JITSI');
        $payload = array(
            "context" => array(
                "user" => array(
                    "avatar" => "avatar",
                    "name" => $userName,
                    "email" => $email,
                )
            ),
            "aud" => "meet-securinets.me",
            "iss" => "meet-securinets.me",
            "sub" => "meet-securinets.me",
            "room" => $name,
            "moderator" => $isModerator
        );
        return JWT::encode($payload, $key);
    }
 
    public function getAllUsersByWorkshopId($workshop_id)
    {
        return User::whereHas('workshops', function ($q) use ($workshop_id) {
            $q->where('Workshop.workshop_id', '=', $workshop_id);
        })
            ->with(['user_workshops' => function ($q) use ($workshop_id) {
                $q->where('workshop_id', '=', $workshop_id);
            }])
            ->get();
    }

    public function getuserWorkshopByWorkshopId($workShop_id)
    {

        return WorkshopUser::where('workshop_id', '=', $workShop_id)
        ->with(['user','workshop_user_evaluation'])
        ->get();
    }

    public function savePresence(Request $request, $workShop_id)
    {
        $workshop_users = $this->getuserWorkshopByWorkshopId($workShop_id);
        $users_presence = $request->all();
        for ($i = 0; $i < sizeof($workshop_users); $i++) {
            if ($workshop_users[$i]->is_present == 0 && $users_presence[$i]['is_present'] != 0) {
            
                $workshop_users[$i]->user->points+=20;
                $workshop_users[$i]->user->points += $this->caculateUserNote($users_presence[$i]['workshop_user_evaluation'],$workshop_users[$i]->workshop_user_evaluation);
            }
             else if ($workshop_users[$i]->is_present == 1 && $users_presence[$i]['is_present'] == 0 )  {
                $workshop_users[$i]->user->points -= 20;
                $workshop_users[$i]->user->points -= $this->caculateUserNote($users_presence[$i]['workshop_user_evaluation'],$workshop_users[$i]->workshop_user_evaluation);
                
            }  else if ($workshop_users[$i]->is_present == 1 && $users_presence[$i]['is_present'] != 0) {
                $oldNote = $this->caculateUserNote($workshop_users[$i]->workshop_user_evaluation,$workshop_users[$i]->workshop_user_evaluation,true);
                 $newNote = $this->caculateUserNote($users_presence[$i]['workshop_user_evaluation'],$workshop_users[$i]->workshop_user_evaluation);
                 $workshop_users[$i]->user->points += (float) ($newNote - $oldNote);
            }
            $workshop_users[$i]->user->update();
            $workshop_users[$i]->is_present = $users_presence[$i]['is_present'];
            $workshop_users[$i]->update();
        }
    }

    public function caculateUserNote($workshop_user_evaluation, $old_workshop_user_evaluation,$isEdit = false) {
        $note = 0;
        if ( sizeof($workshop_user_evaluation) > 0) {
        for($i = 0 ; $i<sizeof($workshop_user_evaluation);$i++) {
            $note += $workshop_user_evaluation[$i]['note']; 
            if (!$isEdit) {
            $old_workshop_user_evaluation[$i]['note'] = $workshop_user_evaluation[$i]['note']; 
            $old_workshop_user_evaluation[$i]->update();
            }
        }
        $note = $note / sizeof($workshop_user_evaluation);
    }
        return $note;
    }

}