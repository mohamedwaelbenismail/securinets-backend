<?php

namespace App\Services;
use Illuminate\Support\Facades\Storage;

class SharedServices
{
    public function generateRandomString()
    {

        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < 15; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;

    }
    public function delete_file($path_file, $media)
    {
        $chemin = config($media);
        $path = $chemin . '/' . $path_file;
        Storage::delete($path);

        return true;

    }

}