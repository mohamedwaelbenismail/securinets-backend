<?php

namespace App\Services;

use App\Models\Membership;
use App\Models\Workshop;
use App\Models\WorkshopUser;
use App\Models\User;
use Illuminate\Support\Facades\Mail;

class UserServices
{

 public function addUSer($firstName, $lastName, $email, $points, $password,$memberhip_id) {
        $user = new User();
        $user->first_name = $firstName;
        $user->last_name = $lastName ;
        $user->email = $email;
        $user->points = 0;
        $user->privilege_id = 3 ;
        $user->email_verified = 0;
        $user->password = bcrypt($password);
        $user->membership_id = $memberhip_id;
        $user->save();
        return $user;
    }


    public function updateUser($user,  $firstName, $lastName, $password, $path,$checked ) {
        $user->first_name = $firstName;
        $user->last_name = $lastName ;
        if ($checked) {
        $user->password = bcrypt($password);
        }
        $user->url_photo = $path ? $path : $user->url_photo;
        $user->update();
        return $user;
    }


    public function getUserWorkshops($user_id)
    {
        return Workshop::whereHas('workshop_users', function($q) use($user_id){
            $q->where('user_id', '=', $user_id);})->get();
    }

    public function getMembershipByIndex($membership_index) {
        return Membership::where('membership_index','=',$membership_index)->first();
    }
    public function getUserByMembershipId($membership_id) {
        return User::where('membership_id','=',$membership_id)->first();
    }

    public function getUserByEmail($email)
    {
        $email = strtolower($email);
        return User::whereRaw('lower(email) = (?)', ["{$email}"])
            ->select(['first_name','last_name','email','email_verified','privilege_id','user_id','membership_id'])
            ->first();
    }
    public function getUserById($user_id) {
        return User::where('user_id','=',$user_id)->first();
    }
    public function getUserByIdAndVerificationCode($user_id,$verification_code) {
        $fieldsToMatch = ['user_id' => $user_id, 'verification_code' => $verification_code];
        return User::where($fieldsToMatch)->first();
    }

   

    public function retrieveUserFromToken()
    {
        try {
            return auth()->user();
        } catch (\Tymon\JWTAuth\Exceptions\TokenExpiredException $e) {
            $refreshed = JWTAuth::refresh(JWTAuth::getToken());
            $user = JWTAuth::setToken($refreshed)->toUser();
            header('Authorization: Bearer ' . $refreshed);
            return $user;
        } catch (\Tymon\JWTAuth\Exceptions\TokenInvalidException $e) {
            return null;
        } catch (\Tymon\JWTAuth\Exceptions\JWTException $e) {
            return null;
        }
    }

   public function sendForgetPasswordMail($user) {
        $randomString = $this->generateRandomString();
       try {
    Mail::send([],[], function($message) use($user,$randomString) {
        $message->from(env('MAIL_USERNAME'), env('MAIL_FROM_NAME'));
        $message->subject('forget_password');
        $message->setBody(view('forget_password')->with(
                        ['link' =>
                        UrlUtils::getFrontUrl() . '/auth/reset-password?&qrlfd=' . (($user->user_id * 1589963) + 5849)
                         .'&verification_code='.$randomString,
                         'user_name' => $user->first_name . '  ' . $user->last_name]
                    ),
         'text/html');
        $message->to($user->email)->subject('forget_password');
    });
    } catch (\Exception $exception) {
     
    return $exception->getMessage();
    }
        $user->verification_code = $randomString;
        $user->reseted_at = date('y-m-d H:i:s');
        $user->update();
        return 1 ;

    }
    public function contactUsMail($name , $mail_from , $content) {
        try {
            Mail::send([],[], function($message) use($name,$mail_from,$content) {
                $message->from(env('MAIL_USERNAME'), env('MAIL_FROM_NAME'));
                $message->subject('contact');
                $message->setBody(view('contact')->with(['mail_from' => $mail_from , 'name' => $name, 'message' => $content]),'text/html');
                $message->to('contact@securinets.com')->subject('contact');
            });
            } catch (\Exception $exception) {
             
            return $exception->getMessage();
            }
                return 1 ;
        
    }
    public function sendEmailConfirmation($user) {
        try {
     Mail::send([],[], function($message) use($user) {
         $message->from(env('MAIL_USERNAME'), env('MAIL_FROM_NAME'));
         $message->subject('emailConfirmation');
         $message->setBody(view('emailConfirmation')->with(
            'link' ,UrlUtils::getBackendUrl().'/users/emailConfirmation?qrifdolp='.(($user->user_id*25698) + 1457)),
             'text/html');
         $message->to($user->email)->subject('Email_confirmation');
     });
     } catch (\Exception $exception) {
      
     return $exception->getMessage();
     }
         return 1 ;
 
     }

     public function generateRandomString() {
      
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < strlen($characters); $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    
  }
  public function getAllUserByPrivilege($privilegeId,$offset,$perPage) {
     $total = $users =  User::where('email_verified','=',1)
         ->where('privilege_id','=',$privilegeId)
         ->orderBy('points', 'DESC')->count();

     $users =  User::where('email_verified','=',1)
         ->where('privilege_id','=',$privilegeId)
         ->orderBy('points', 'DESC')->offset($offset)->limit($perPage)->get();
      $from = $offset;
      $to = $offset + $users->count();
     return ["data" => $users , "total" => $total , "from" => $from, "to" => $to];
  }
  public function checkPrivilege($privilegeId){
      $privilegeIds = array("1","2","3");
        if(in_array($privilegeId, $privilegeIds, true)) {
            return true;
        }
    return false;
}

}