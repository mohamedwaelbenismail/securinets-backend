<?php

namespace App\Http\Controllers;

use App\Models\User;
use App\Models\WorkshopUser;
use App\Services\ItemEvaluationService;
use App\Services\UserServices;
use App\Services\WorkshopServices;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use App\Services\SharedServices;


class WorkshopController extends Controller
{
    protected $workShopServices; 
    protected $userServices;
    protected $sharedServices;
    protected $ItemEvaluationService;

    function __construct(
        WorkshopServices $workShopServices,
        SharedServices $sharedServices,
        ItemEvaluationService $itemEvaluationService,
        UserServices $userServices)
    {
        $this->workShopServices = $workShopServices ;
        $this->userServices = $userServices;
        $this->sharedServices = $sharedServices;
        $this->ItemEvaluationService = $itemEvaluationService;

    }
    public function createWorkShop(Request $request) {
       
        $user = $this->userServices->retrieveUserFromToken();
        if ($user->privilege_id != 1 && $user->privilege_id != 2 ) {
            return response()->json('unauthorized',401);
        }
        $validator = Validator::make($request->all() , [
            'name' => 'required',
            'description' => 'required',
            'class_room' => 'required',
            'start_date' => 'required',
            'duration' => 'required',
            'max_places' => 'required',
        ]);
        if ($validator->fails())
            return response()->json($validator->errors(),400);
        $path = null;
        if ($request->hasFile('photo')) {
            $file = $request->file('photo');
            $chemin = config('media.workshop-photo');
            $path = $file->store($chemin);
            $path = str_replace('workshop-photo/', '', $path);
        } 
   
        $workShop = $this->workShopServices->addWorkshop(
            $request->input('name'),
            $request->input('description'),
            $request->input('class_room'),
            $request->input('start_date'),
            $request->input('duration'),
            $path,
            $request->input('max_places'),
            $user->user_id
        );
        $json = $request->input('grids');
        $grids = json_decode($json,true);
        foreach ($grids as $grid) {
            $item = $this->ItemEvaluationService->addItem($grid['label'],$user->user_id);
            $this->ItemEvaluationService->addWorkShopEvaluation($item->item_evaluation_id,$workShop->workshop_id);
        }

        return response()->json( 'workshop added',200);
    }
   public function getWorkshopPhoto($path) {
    $chemin = config('media.workshop-photo');
    return response()->download(storage_path('app/' . $chemin . "/" . $path));
   }

    public function deleteWorkShop($workshop_id) {
        if (!$workShop = $this->workShopServices->getWorkShopById($workshop_id)) {
            return response()->json('workshop not found ',404);
        }
        $user = $this->userServices->retrieveUserFromToken();
        if ($user->user_id != $workShop->moderator_id) {
            return response()->json('unauthorized',401);
        }
        $workShop->delete();
        return response()->json('workshop deleted',200);
    }

    public function editWorkShop($workshop_id,Request $request) {
        if (!$workShop = $this->workShopServices->getWorkShopById($workshop_id)) {
            return response()->json('workshop not found ',404);
        }
        $user = $this->userServices->retrieveUserFromToken();
        if ($user->user_id != $workShop->moderator_id) {
            return response()->json('unauthorized',401);
        }
        $path = null;
        if ($request->hasFile('photo')) {
            $file = $request->file('photo');
            $chemin = config('media.workshop-photo');
            $path = $file->store($chemin);
            $path = str_replace('workshop-photo/', '', $path);
            if($workShop->url_photo) {
                $this->sharedServices->delete_file($workShop->url_photo,'media.workshop-photo');
            }
        } 
        $this->workShopServices->editWorkShop(
            $workShop,
            $request->input('name'),
            $request->input('description'),
            $request->input('class_room'),
            $request->input('start_date'),
            $request->input('duration'),
            $path
        );
        $json = $request->input('grids');
        $grids = json_decode($json,true);
        $oldGrids = $workShop->item_evaluation;
        foreach ($grids as $key => $grid) {
            if (!isset($grid['item_evaluation_id'])) {
                $item = $this->ItemEvaluationService->addItem($grid['label'],$user->user_id);
                $this->ItemEvaluationService->addWorkShopEvaluation($item->item_evaluation_id,$workshop_id);
            } else {
                if ($grid['label'] != $oldGrids[$key]->label) {
                    $this->ItemEvaluationService->editItem($oldGrids[$key],$grid['label']);
                }
            }
        }
        foreach($oldGrids as $oldGrid) {
            $isExist = false;
            foreach($grids as $grid) {
                if (isset($grid['item_evaluation_id']) && $grid['item_evaluation_id'] == $oldGrid->item_evaluation_id) {
                    $isExist = true ;
                break;
                }
            }
            if (!$isExist) {
                $oldGrid->delete();
            }
        }

        
        
        return response()->json('workshop updated successfully',200);
    }
    
    public function joinWorkshop(Request $request) {
        $workshop_id = $request->input('workshop_id',0);
        if (!$user = $this->userServices->retrieveUserFromToken()){
            return response('no user found',404);
        };
        
        $workshop = $this->workShopServices->getWorkShopById($workshop_id);
        if (!$workshop) {
            return response()->json('workshop not found',404);
        }

        if ($workshop->available_places <=0) {
            return response()->json('workshop is full',400);
        }

        if ($this->workShopServices->getUserByUserIdByWorkshopId($user->user_id,$workshop_id)) {
            return response()->json('Already joined workshop',400);
    }
        if ($workshop->moderator_id === $user->user_id) {
            return response()->json('You cannot join your own workshop',400);
        }
        $this->workShopServices->affectWorkshopToUSer($user->user_id,$workshop_id, $workshop);
        return response()->json('joined successfully',200);
    }

    public function getMyPublishedWorkshops() {
        $user = $this->userServices->retrieveUserFromToken();
        if ($user->privilege_id !=1 && $user->privilege_id !=2 ) {
            return response()->json('unauthorized',401);
        }
        $my_workshops = $this->workShopServices->getWorkShopsByModeratorId($user->user_id);
        return response()->json($my_workshops,200);
    }

    public function getAllWorkshops(Request $request) {
        $offset = $request->query('offset', 0);
        $perPage = $request->query('perPage', 2);
        $search = $request->query('search', '');
        $moderator_id = $request->query('moderateur_id');
        $user = $this->userServices->retrieveUserFromToken();
        $user_id = $user ? $user->user_id : null;
        $workshops = $this->workShopServices->getAllWorkshops($offset,$perPage,$search,$user_id,$moderator_id);
        return response()->json($workshops,200);
    }

    public function getWorkshopById(Request $request ,$workshop_id) {

        $from = $request->query('from');
        if (!$workshop = $this->workShopServices->getWorkShopById($workshop_id)) {
            return response()->json('workshop not found ',404);

        }
        $isModerator = 0;
        if ($admin = $this->userServices->retrieveUserFromToken() ) {
           $isModerator = $admin->user_id == $workshop->moderator_id ? 1 : 0;
           if ($from == 'edit' && !$isModerator) {
               //test de securité c  tt 
               return response()->json('not your workshop',400);
           }
        }
        $workshop['isModerator'] = $isModerator;
        return response()->json($workshop,200);
    }

    public function createRoomAuth($workshop_id) {
        if (!$workShop = $this->workShopServices->getWorkShopById($workshop_id)) {
            return response()->json('workshop not found ',404);

        }
        if (!$user = $this->userServices->retrieveUserFromToken()) {
            return response()->json('no user found',400);
        }
        $isModerator = false ;
        if ($user->user_id == $workShop->moderator_id) {
            $isModerator = true ;
        } else {
            if (!$workshop_user = $this->workShopServices->getUserByUserIdByWorkshopId($user->user_id,$workshop_id) ) {
                return response()->json('you have to register to this workshop',400);
            }

            }
        
        $token = $this->workShopServices->createToken(
            $user->email,
            'room '.$workshop_id,
            $isModerator,
            $user->user_name
        );
        return response()->json($token,200);
    }

    public function quitWorkshop($workshopId){
        if (!$user = $this->userServices->retrieveUserFromToken()) {
            return response()->json(['error' => 'user not found'], 404);
        }
        $workshop = $this->workShopServices->getWorkShopById($workshopId);
        if (!$workshop) {
            return response()->json('workshop not found',404);
        }
        // condition ala el wa9t lena ( tu peux pas quitter un workshop déjà passer)
        $user_id=$user->user_id;
        
        WorkshopUser::where('user_id','=',$user_id)
        ->where('workshop_id','=',$workshopId)->delete();
        
        $workshop->available_places = $workshop->available_places + 1;
        $workshop->update();
        return response()->json('you have quit this workshop',200);


    }

    public function getAllUsersByWorkshopId($workshopId){


        if (!$workshop = $this->workShopServices->getWorkShopById($workshopId)) {
            return response()->json('workshop not found',404);
        }
        $users=$this->workShopServices->getAllUsersByWorkshopId($workshopId);
        return response()->json($users,200);
    }

    public function savePresence (Request $request,$workshop_id){
        if (!$workshop = $this->workShopServices->getWorkShopById($workshop_id)) {
            return response()->json('workshop not found',404);
        }
        $user = $this->userServices->retrieveUserFromToken();
        if ($user->user_id != $workshop->moderator_id) {
            return response()->json('unauthorized',401);
        }
        if(!$request)
            return response()->json('workshop users not found',404);

        $this->workShopServices->savePresence($request,$workshop_id);
        return response()->json('presence modified successfully',200);
    }
}
