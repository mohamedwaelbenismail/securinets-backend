<?php

namespace App\Http\Controllers;

use App\Services\SharedServices;
use App\Services\UrlUtils;
use App\Services\UserServices;
use App\Services\WorkshopServices;
use DateTime;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
class UserController extends Controller
{
    protected $userServices ;
    protected $sharedServices;
    protected $workshopSerives;

    function __construct(UserServices $userServices, SharedServices $sharedServices, WorkshopServices $workshopSerives)
    {   
        $this->userServices = $userServices;
        $this->sharedServices = $sharedServices;
        $this->workshopSerives = $workshopSerives;

    }
    public function registerUser(Request $request) {
        if (!($request->has('first_name') &&
        $request->has('membership_index') && 
        $request->has('last_name')&&
        $request->has('email') &&
        $request->has('points')&&
        $request->has('privilege_id')&&
        $request->has('password')
        ) ) {
            return response()->json(['response' => 'Some fields are missing']);
        }
        if (! $this->checkPwd($request->input('password'))) {
            return response()->json(['response' => 'Error with your pwd']);
        }
        if (!$this->userServices->getMembershipByIndex($request->input('membership_index'))) {
            return response()->json(['response' => 'Please type a valid membership id, if the problem persists please contact us'],400);
        }
        $membership = $this->userServices->getMembershipByIndex($request->input('membership_index'));
        if ($this->userServices->getUserByMembershipId($membership->membership_id)){
            return response()->json(['response' => 'This membership is already used'],400);
        }
        if ($user = $this->userServices->getUserByEmail(
            $request->input('email')
        )) {
            if ($user->membership_id) {
            return response()->json(['response' => 'You have already an account'],400);
            } else {
                $user->membership_id = $membership->membership_id;
                $user->update();
                return response()->json(['response' => 'user added successfully'],200);
            }
        }
        try {  
         $user = $this->userServices->addUSer(
            $request->input('first_name'),
            $request->input('last_name'),
            $request->input('email'),
            $request->input('points'),
            $request->input('password'),
            $membership->membership_id
        );
        //todo: send email confirmation
        $this->userServices->sendEmailConfirmation($user);
    } catch(Exception $e) {
        return response()->json($e->getMessage(),400);
    }
        return response()->json(['response' => 'user added successfully'],200);

    }
    public function updateUser(Request $request) {
        $checked = $request->query('checked') === 'true' ? true : false;
        if ($checked) {
        if (!($request->has('first_name') &&
            $request->has('last_name')&&
            $request->has('password') &&
            $request->has('old_password')
        ) ) {
            return response()->json(['response' => 'Some fields are missing'],400);
        }
        if (! $this->checkPwd($request->input('password'))) {
            return response()->json(['response' => 'Choose a strong password'],500);
        }
    } else {
        if (!($request->has('first_name') &&
        $request->has('last_name') && 
        $request->has('old_password') 
        ))  {
        return response()->json(['response' => 'Some fields are missing'],400);
    }
    }
        try {

            if (!$user = $this->userServices->retrieveUserFromToken()) {
                return response()->json('user not found',404);
            }
            if (!Hash::check($request->input('old_password'),$user->password)) {
                return response()->json(['message' => 'old password is wrong'],500);
            }
            $path = $user->url_photo;
            if ($request->hasFile('photo')) {
                $file = $request->file('photo');
                $chemin = config('media.users-photo');
                $path = $file->store($chemin);
                $path = str_replace('users-photo/', '', $path);
                if($user->url_photo) {
                    $this->sharedServices->delete_file($user->url_photo,'media.users-photo');
                }
            }
            $userUpdated = $this->userServices->updateUser(
                $user,
                $request->input('first_name'),
                $request->input('last_name') ,
                $request->input('password'),
                $path,
                $checked
            );
        } catch(Exception $e) {
            return response()->json($e->getMessage(),400);
        }
        return response()->json($userUpdated, 200);

    }
    public function getUserPhoto($path) {
        $chemin = config('media.users-photo');
        return response()->download(storage_path('app/' . $chemin . '/' . $path));
    }

    public function isAdmin() {
        if (!$user = $this->userServices->retrieveUserFromToken()) {
            return response()->json('no user found',404);
        }
      
        if ($user->privilege_id != 1 && $user->privilege_id!= 2  ) {
            return 'false';
        }
        return 'true';
    }

    public function getUserByToken() {
        try {
        if (!$user = $this->userServices->retrieveUserFromToken()) {
            return response()->json('no user found',404);
        }
        return response()->json($user,200);}
        catch (Exception $e) {
            return response()->json(['response' => $e->getMessage()], 400);
        }
    }

    public function getUserById($user_id) {
        $user_id = ($user_id - env('num1'))/env('num2');
        if (!$user = $this->userServices->getUserById($user_id)) {
            return response()->json('user not found',404);
        }
        return response()->json($user,200);
    }
    public function contact(Request $request) {
        if (! ($request->has('message')  || $request->has('email') || $request->has('name'))) {
            return response()->json('fields are missing',404);
        }
        $this->userServices->contactUsMail(
            $request->input('name'),
            $request->input('email'),
            $request->input('message')
        );
        return response()->json('message sent',200);
    }
    public function getUserByIdAndVerificationCode($user_id,$verification_code) {
        if (!$user = $this->userServices->getUserByIdAndVerificationCode($user_id,$verification_code)) {
            return response()->json('no user found',404);
        }
        $timeUserUpdated = new DateTime($user->reseted_at);
        $timeNow =  new DateTime(date('y-m-d H:i:s'));
        $interval = ($timeUserUpdated->diff($timeNow));
        if ($interval->h > 1) { //plus qu'une heure ;
            return response()->json('not allowed',400);
        }
        return response()->json($user,200);
    }


    public function emailConfirmation(Request $request) {
        // todo: add more security;
        $user_id = $request->query('qrifdolp');
        $user_id = ($user_id - 1457)/25698;
        if (!$user = $this->userServices->getUserById($user_id)) {
            return response()->json('no user found',404);
        }
        if ($user->email_verified == 1) {
            return response()->redirectTo(UrlUtils::getFrontUrl());
        }
        $user->email_verified = 1 ;
        $user->update();
        
        return response()->redirectTo(UrlUtils::getLoginFront().'?confirm=true');
    }
    public function getUserWorkshops(Request $request) {

        $user_id = 3; //retreiveUserFromToken;
        $workshops = $this->userServices->getUserWorkshops($user_id);
        return response()->json($workshops,200);
    }
    
    public function getUserByEmail(Request $request)
    {
        $email = $request->input('email');

        if (!$user = $this->userServices->getUserByEmail($email)) {
            return response()->json(['error' => 'user not found'], 404);
        }

        return response()->json($user);
    }
    public function forgetPassword(Request $request) {
        if (!($request->has('email'))) {
            return response()->json('Email field is required',400);
        
        }
        if (!$user = $this->userServices->getUserByEmail($request->input('email'))) {
            return response()->json('no user found',404);
        }
        //send email that redirects the user to a front-office link, in which he can change his pwd
        return $this->userServices->sendForgetPasswordMail($user);
        
    }
    public function resetPassword($user_id,$verification_code,Request $request){
        if(!$request->has('password')){
            return response()->json('password is required',400);
        }
        if ($verification_code == "null") {
            return response()->json('not allowed',400);
        }
        if (!$user = $this->userServices->getUserByIdAndVerificationCode($user_id,$verification_code)){
            return response()->json('no user found',404);
        }
       
        if (!$this->checkPwd($request->input('password'))) {
            return response()->json('check your password pattern',400);
        }
        $user->password = bcrypt($request->input('password'));
        $user->verification_code = null;
        $user->update();
        return response()->json('password updated successfully',200);

    }

    public function getAllUserByPrivilege(Request $request, $privilegeId) {
        $offset = $request->query('offset', 0);
        $perPage = $request->query('perPage', 10);
        if (!$this->userServices->checkPrivilege($privilegeId)) {
            return response()->json('wrong privilege',404);
        }
        try {
            if (!$user = $this->userServices->retrieveUserFromToken()) {
                return response()->json('no user found',404);
            }
            if (($privilegeId == 2 || $privilegeId == 1) && $user->privilege_id == 3)  {
                return response()->json('access denied',401);
            }
            $users = $this->userServices->getAllUserByPrivilege($privilegeId,$offset,$perPage);
            return response()->json($users,200);}
        catch (Exception $e) {
            return response()->json(['response' => $e->getMessage()], 400);
        }

    }

    private function checkPwd($pwd) {
        if (strlen($pwd) < 8  ||
        !(preg_match('/[A-Z]/',$pwd)) ||
        !(preg_match('/[a-z]/',$pwd)) || 
        !(preg_match('/[0-9]/',$pwd))
        ) 
           return 0 ;
        
        return 1 ;
    }

}
