<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class workshop_user_evaluation extends Model
{
    protected $table = 'Workshop_User_Evaluation';
    protected $primaryKey = 'workshop_user_evaluation_id';
    protected $fillable = ['item_evaluation_id','workshop_user_id','note'];
    public $timestamps = true;
    protected $dates = ['created_at', 'updated_at', 'deleted_at'];
}
