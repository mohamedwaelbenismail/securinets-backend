<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class WorkshopUser extends Model
{
    protected $table = 'Workshop_User';
    protected $primaryKey = 'workshop_user_id';
    protected $fillable = ['workshop_id','user_id','is_present'];
    public $timestamps = true;
    protected $dates = ['created_at', 'updated_at', 'deleted_at'];

    public function user()
    {
        return $this->belongsTo('App\Models\User', 'user_id', 'user_id');
    }

    public function workshop()
    {
        return $this->belongsTo('App\Models\Workshop', 'workshop_id', 'workshop_id');
    }

    public function workshop_user_evaluation() {
        return $this->hasMany(workshop_user_evaluation::class,'workshop_user_id','workshop_user_id');
    }


}
