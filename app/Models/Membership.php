<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Membership extends Model {

    protected $table = 'Membership';
    protected $primaryKey = 'membership_id';
    protected $fillable = ['membership_index'];
    public $timestamps = true;
    protected $dates = ['created_at', 'updated_at', 'deleted_at'];
}