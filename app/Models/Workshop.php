<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Workshop extends Model {

    protected $table = 'Workshop';
    protected $primaryKey = 'workshop_id';
    protected $fillable = ['label','moderator_id','class_room','start_date','duration','max_places','available_places', 'url_photo'];
    public $timestamps = true;
    protected $dates = ['created_at', 'updated_at', 'deleted_at'];


    public function workshop_users()
    {
        return $this->hasMany(WorkshopUser::class, 'workshop_id', 'workshop_id');

    }
    public function moderator() {
        return $this->hasOne(User::class, 'user_id','moderator_id');
    }

    public function user() {
        return $this->belongsToMany(User::class,WorkshopUser::class,'workshop_id','user_id');
    }

    public function item_evaluation() {
        return $this->belongsToMany(ItemEvaluation::class, workshop_evaluation::class,'workshop_id','item_evaluation_id');
    }

}